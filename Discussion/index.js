console.log("hello world")
// Comments in Javascript
// - Two write comments, we use two forward slash for single line comments and two forward slash and two asterisk for multi-line comments
//This is a single line comment.
/*
This is a multi-line comment.
To write them, we add tow asteris k inside of the forward and write comments in between them.
*/
// VARIABLE
/*
	- Varaibles contain values that can be changes over the execution time of the program. 
	- To declare a variable, we use the "let" keywords
*/
/*let productName = 'desktop computer';
console.log(productName);
productName = 'cellphone'
console.log(productName)
*/
// CONSTANTS
/*
	- Use constant for values that will not chnage.
*/

//DATA TYPES
// 1. String
/*
	- Strings are a series of characters that creat a word, a phrase,sentence, or anything related to "TEXT" 
	- String in Javascript can be written using a single quote ('') or double quote ("")
	-On  other programming languages, only the double quote can be used for creating strings.

*/
let country ='Philippines';
let province ="Metro Manila";

// CONCATENATION
console.log(country + ", " + province);

// 2. Numbers
/*
	- Include integers/whole numbers, decimal numbers fraction, exponential notations
*/

let headCount = 26;
console.log(headCount);

let grade = 98.7;
console.log(grade);

let planetDistance = 2e10;
console.log(planetDistance);

let PI = 22/7;
console.log(PI);
console.log(Math.PI)

// 3. BOOLEAN
/*
	- Boolean values are logical values.
	- Boolean values can either "true" or "false"
*/

let isMarried = false;
let isGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("isGoodConduct:" + isGoodConduct);

// 4. OBJECTS
// ARRAY
	/*
	- They are a special kind of data that stores multiple values.
	- They are a special type of an object.
	- They can store different data types but is normaly used to store similar data types
	*/
	// Syntax:
	// let/const arrayName =[ElemetsA, ElementsB, ElementsC...]

let grades = [98.7, 92.1, 90.2, 94.6 ];
console.log(grades[0]);

let userDetails = ["JOhn", "Smith", 32, true];
console.log(userDetails);

// Objects Literals
/*
	- Onjects are another special kind of data type that mimic real world objects/items
	- They are used to create complex data that contains pieces of information that are relevant to each other.
	- Every individual piece of information if called a property of onjects

	Syntax:
	let/const objectName = {
	propertyA: value,
	propertyB: value,
	}
*/

	let personDetails = {
		fullname: 'Janel Dela Cruz',
		age: 35,
		isMarried:false,
		contact: ['+639171597616' , '023128373'],
		address: {
			houseNumber: '345',
			city: 'Manila'
		}
	}
console.log(personDetails);

//NULL
/*
	-It is used to intentionally express the absense of a value
*/

let spouse = null;
console.log(spouse);

// 6. UNDEFINED
/*
	- Represents the states of variable that has been declared but without an assigned value
*/

let vacationPlace;
console.log(vacationPlace);