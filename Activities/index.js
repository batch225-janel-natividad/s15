	let personDetails = {
		firstName: 'John',
		LastName: 'Smith',
		Age: 30,
		Hobbies: ['Biking' , 'Mountain Climbing', 'Swimming'],
		WorkAddress: {
			houseNumber: '32',
			street: 'Washington',
			city: 'Lincoln',
			state: 'Nebraska'
		}
	}

	console.log("First Name: " + personDetails.firstName);
	console.log("Last Name: " + personDetails.LastName);
	console.log("Age: " + personDetails.Age);
	console.log("Hobbies: " + personDetails.Hobbies);
	console.log("Work Address:");
	console.log(personDetails.WorkAddress);


	let fullName = "Steve Rogers";
	console.log("My full name is" + fullName);

	let age = 40;
	console.log("My current age is: " + age);
	
	let friends = ["Tony","Bruce","Thor,Natasha","Clint","Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let fullName1 = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName1);

	const lastLocation = "Arctic Ocean";
	console.log("I was found frozen in: " + lastLocation);
